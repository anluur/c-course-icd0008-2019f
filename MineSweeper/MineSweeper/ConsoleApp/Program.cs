﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleUI;
using DAL;
using GameEngine;
using MenuUI;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            Console.WriteLine("Hello Game!");

            var difficulty = new Menu(1)
            {
                Title = "Choose game difficulty.",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Easy",
                            //game = new Game(9, 9, 10);
                            CommandToExecute = (() => SetDifficulty(GameDifficulty.Easy))
                        }
                    },
                    {
                        "2", new MenuItem()
                        {
                            Title = "Medium",
                            //game = new Game(16, 16, 40);
                            CommandToExecute = (() => SetDifficulty(GameDifficulty.Medium))
                        }
                    },
                    {
                        "3", new MenuItem()
                        {
                            Title = "Hard",
                            //game = new Game(16, 30, 99);
                            CommandToExecute = (() => SetDifficulty(GameDifficulty.Hard))
                        }
                    },
                    {
                        "4", new MenuItem()
                        {
                            Title = "Custom",
                            CommandToExecute = (() => SetDifficulty(GameDifficulty.Custom))
                        }
                    }
                }
            };
            
            
            var mainMenu = new Menu(0)
            {
                Title = "MineSweeper Main Menu",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Start a new game",
                            CommandToExecute = (() => TestGame(settings: _settings))
                        }
                    },
                    {
                        "2", new MenuItem()
                        {
                            Title = "Load game",
                            CommandToExecute = RunLoadGameMenu
                        }
                    },
                    {
                        "3", new MenuItem()
                        {
                            Title = "Difficulty",
                            CommandToExecute = (() => difficulty.Run(true))
                        }
                    },
                }
            };


            mainMenu.Run(true);
        }

        private static string RunLoadGameMenu()
        {
            List<GameSave> savedGames = new List<GameSave>();
            using (var ctx = new AppDbContext())
            {
                savedGames = ctx.GameSaves.OrderBy(save => save.SaveName).ToList();
            }
            
            var savedGamesIndexes = Enumerable.Range(1, savedGames.Count + 1);

            var loadGameMenu = new Menu(1)
            {
                Title = "Choose a saved game you want to load.",
                // WARNING HEAVY LAMBDAS AHEAD!!!
                MenuItemsDictionary = savedGamesIndexes
                    // Zip into pair of array item index and array item
                    .Zip(savedGames, (index, savedGame) => new {index, savedGame})
                    // Convert to dictionary of menu items 
                    .ToDictionary(item => (item.index).ToString(), item => 
                        new MenuItem()
                        {
                            Title = item.savedGame.SaveName,
                            CommandToExecute = (() => TestGame(item.savedGame.SaveName))
                        })
            };
            return loadGameMenu.Run(true);
        }

        private static GameDifficulty _difficulty = GameDifficulty.Easy;
        private static GameSave? _settings = null;
        
        private static string SetDifficulty(GameDifficulty difficulty)
        {
            if (difficulty == GameDifficulty.Custom)
            {
                _settings = SetUpCustomGame();
            }
            else
            {
                _difficulty = difficulty;
            }

            return "M";
        }

        static Game LoadGame(string saveName)
        {
            GameSave gameSave;
            Console.WriteLine(saveName);
            using (var ctx = new AppDbContext())
            {
                gameSave = ctx.GameSaves.Single(save => save.SaveName == saveName);
            }
            return new Game(gameSave);
        }

        private static GameSave? SetUpCustomGame()
        {
            var userCanceled = false;
        
            do
            {
                var boardHeight = 0;
                (boardHeight, userCanceled) = GetUserIntInput("Enter board height", 9, 50, 0);
                if (!userCanceled)
                {
                    var boardWidth = 0;
                    (boardWidth, userCanceled) = GetUserIntInput("Enter board width", 9, 50, 0);
                    if (!userCanceled)
                    {
                        var mineAmount = 0;
                        (mineAmount, userCanceled) = 
                            GetUserIntInput("Enter mine amount", 1, boardHeight * boardWidth - 1, 0);
                        if (!userCanceled)
                        {
                            _difficulty = GameDifficulty.Custom;
                            var gameSave = new GameSave()
                            {
                                BoardHeight = boardHeight,
                                BoardWidth = boardWidth,
                                MineCount = mineAmount,
                                Board = GameBoardJsonConverter.Serialize(new Cell[boardHeight, boardWidth])
                            };
                            Console.WriteLine(gameSave.MineCount);
                            return gameSave;
                        }
                    }
                }

            } while (!userCanceled);
            
            return null;
        }

        static string TestGame(string? saveName = null, GameSave? settings = null)
        {
            Game game;
            if (_difficulty == GameDifficulty.Custom && settings != null)
            {
                game = new Game(settings);
                Console.WriteLine(game.MineAmount);
            }
            else if (saveName == null)
            {
                game = new Game(_difficulty);
            }
            
            else
            {
                game = LoadGame(saveName);
            }
            

            var flag = false;
            do
            {
                Console.Clear();
                GameUI.PrintBoard(game);
                
                Console.WriteLine("To flag cell enter: F");
                flag = Console.ReadLine()?.Trim().ToUpper() == "F";
                
                var userXint = 0;
                var userYint = 0;
                var userCanceled = false;
                
                (userXint, userCanceled) = GetUserIntInput("Enter X coordinate", 1, game.BoardWidth, 0);
                if (!userCanceled)
                {
                    (userYint, userCanceled) = GetUserIntInput("Enter Y coordinate", 1, game.BoardHeight, 0);
                }

                if (userCanceled)
                {
                    Console.WriteLine(saveName);
                    var quitGameMenu = GetSaveAndQuitMenu(game, saveName);
                    return quitGameMenu.Run(false);
                }

                if (flag)
                {
                    game.Flag(userYint-1, userXint-1);
                    flag = false;
                }
                else
                {
                    game.Move(userYint-1, userXint-1);
                }

            } while (!game.GameOver);
            
            Console.Clear();
            GameUI.PrintBoard(game);

            var gameOverMenu = GetGameOverMenu(game.PlayerWon);
            return gameOverMenu.Run(false);
        }

        static (int result, bool wasCanceled) GetUserIntInput(string prompt, int min, int max, int? cancelIntValue = null, string cancelStrValue = "")
        {
            do
            {
                Console.WriteLine(prompt);
                if (cancelIntValue.HasValue || !string.IsNullOrWhiteSpace(cancelStrValue))
                {
                    Console.WriteLine($"To cancel input enter: {cancelIntValue}" +
                                      $"{(cancelIntValue.HasValue && !string.IsNullOrWhiteSpace(cancelStrValue) ? " or " : "")}" +
                                      $"{cancelStrValue}");
                }

                Console.Write(">");
                var consoleLine = Console.ReadLine();

                if (consoleLine == cancelStrValue) return (0, true);

                if (int.TryParse(consoleLine, out var userInt))
                {
                    if (userInt == cancelIntValue)
                    {
                        return (userInt, true);
                    }

                    if (userInt >= min && userInt <= max)
                    {
                        return (userInt, false);
                    }
                    else
                    {
                        Console.WriteLine($"'{consoleLine}' is out of game board bounds!");
                        continue;
                    }
                }

                Console.WriteLine($"'{consoleLine}' cant be converted to int value!");
            } while (true);
        }

        static Menu GetGameOverMenu(bool playerWon)
        {
            var title =  playerWon ? "You Won!" : "Game Over!";
            var gameOverMenu = new Menu(1)
            {
                Title = title,
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Play again",
                            CommandToExecute = (() => TestGame())
                        }
                    }
                }
            };
            return gameOverMenu;
        }

        static Menu GetSaveAndQuitMenu(Game game, string? saveName)
        {
            var menuItems = new Dictionary<string, MenuItem>();
            var command = "1";
            if (saveName != null)
            {
                var saveAndQuit = new MenuItem()
                {
                    Title = "Save and quit",
                    CommandToExecute = (() => SaveGame(game, saveName))
                };
                menuItems.Add("1", saveAndQuit);
                command = "2";
            }
            
            var saveAs = new MenuItem()
            {
                Title = "Save as",
                CommandToExecute = (() => SaveGame(game))
            };
            menuItems.Add(command, saveAs);
            
            
            var quitGameMenu = new Menu(1)
            {
                Title = "Would you like to save the game?",
                MenuItemsDictionary = new Dictionary<string, MenuItem>(menuItems)
            };
            return quitGameMenu;
        }
        
        static string SaveGame(Game game, string? saveName = null)
        {
            GameSave gameSave;
            if (saveName == null)
            {
                IEnumerable<string> names = new List<string>();
                using (var ctx = new AppDbContext())
                {
                    names = from save in ctx.GameSaves.ToList()
                        select save.SaveName;
                }
               
                do
                {
                    Console.Out.WriteLine("-----------------------------");
                    Console.WriteLine("Please enter your save name.");
                    saveName = Console.ReadLine()?.Trim() ?? "";
                } while (saveName == "" || names.Contains(saveName));

                var currentGameSave = new GameSave
                {
                    SaveName = saveName,
                    BoardHeight = game.BoardHeight,
                    BoardWidth = game.BoardWidth,
                    MineCount = game.MineAmount,
                    Board = GameBoardJsonConverter.Serialize(game.GetBoard()),
                    FlagCount = game.FlagCount,
                    RevealedCellAmount = game.RevealedCellAmount
                };

                using (var ctx = new AppDbContext())
                {
                    ctx.Add(currentGameSave);
                    ctx.SaveChanges();
                }
            }
            else
            {
                using (var ctx = new AppDbContext())
                {
                    gameSave = ctx.GameSaves.Single(save => save.SaveName == saveName);

                    gameSave.Board = GameBoardJsonConverter.Serialize(game.GetBoard());
                    gameSave.FlagCount = game.FlagCount;
                    gameSave.RevealedCellAmount = game.RevealedCellAmount;

                    ctx.GameSaves.Update(gameSave);

                    ctx.SaveChanges();

                }
            }
            
            return "M";
        }
    }
}