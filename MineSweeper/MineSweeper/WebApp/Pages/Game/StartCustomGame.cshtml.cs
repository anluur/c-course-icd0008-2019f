﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using DAL;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class StartCustomGame : PageModel
    {
        
        private readonly AppDbContext _context;

        public StartCustomGame(AppDbContext context)
        {
            _context = context;
        }
        
        public IActionResult OnGet()
        {
            return Page();
        }
        [BindProperty] [Required] [Display(Name = "game name")]
        public string GameName { get; set; }
        [BindProperty] [Display(Name = "board width")] [Range(9, 50, ErrorMessage = "Board width must be between 9 and 50")]
        public int BoardWidth { get; set; }
        [BindProperty] [Display(Name = "board height")] [Range(9, 50, ErrorMessage = "Board height must be between 9 and 50")]
        public int BoardHeight { get; set; }
        [BindProperty] [Display(Name = "mine amount")] [Range(1, 2499, ErrorMessage = "Mine amount must be between 1 and (board width * board height - 1)")]
        public int MineAmount { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var game = new GameEngine.Game(BoardHeight, BoardWidth, MineAmount);
            
            var currentGameSave = new GameSave
            {
                SaveName = GameName,
                BoardHeight = game.BoardHeight,
                BoardWidth = game.BoardWidth,
                MineCount = game.MineAmount,
                Board = GameBoardJsonConverter.Serialize(game.GetBoard()),
                FlagCount = game.FlagCount,
                RevealedCellAmount = game.RevealedCellAmount
            };

            _context.GameSaves.Add(currentGameSave);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Play", new {gameId = currentGameSave.GameSettingsId});
        }
    }
}