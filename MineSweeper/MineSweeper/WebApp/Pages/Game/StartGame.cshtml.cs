﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Game
{
    public class StartGame : PageModel
    {

        private readonly AppDbContext _context;

        public StartGame(AppDbContext context)
        {
            _context = context;
        }
        
        public SelectList GameDifficultySelectList { get; set; }

        public IActionResult OnGet()
        {
            GameDifficultySelectList = new SelectList(Enum.GetValues(typeof(GameDifficulty)));

            return Page();
        }

        [BindProperty] [Required] [Display(Name = "game name")]
        public string GameName { get; set; }
        [BindProperty] public GameDifficulty GameDifficulty { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            
            if (!ModelState.IsValid)
            {
                GameDifficultySelectList = new SelectList(Enum.GetValues(typeof(GameDifficulty)));
                
                return Page();
            }

            if (GameDifficulty == GameDifficulty.Custom)
            {
                return Redirect("./StartCustomGame");
            }
            
            var matchingSaves = await _context.GameSaves.Where(save => save.SaveName == GameName).ToListAsync();

            if (matchingSaves.Count > 0)
            {
                GameDifficultySelectList = new SelectList(Enum.GetValues(typeof(GameDifficulty)));
                
                return Page();
            }
            
            var game = new GameEngine.Game(GameDifficulty);
            
            var currentGameSave = new GameSave
            {
                SaveName = GameName,
                BoardHeight = game.BoardHeight,
                BoardWidth = game.BoardWidth,
                MineCount = game.MineAmount,
                Board = GameBoardJsonConverter.Serialize(game.GetBoard()),
                FlagCount = game.FlagCount,
                RevealedCellAmount = game.RevealedCellAmount
            };

            _context.GameSaves.Add(currentGameSave);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Play", new {gameId = currentGameSave.GameSettingsId});
        }
    }
}