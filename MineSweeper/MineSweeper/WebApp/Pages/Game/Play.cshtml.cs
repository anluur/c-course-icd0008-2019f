﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class Play : PageModel
    {

        private readonly AppDbContext _context;

        public Play(AppDbContext context)
        {
            _context = context;
        }

        public GameEngine.Game Game { get; set; }
        public Cell[,] Board { get; set; }
        public int GameId { get; set; }

        [BindProperty] public bool Flag { get; set; } = false;

        public IActionResult OnGetAsync(int? gameId)
        {
            if (gameId == null)
            {
                return RedirectToPage("./StartGame");
            }
            
            GameId = gameId.Value;
            
            var gameSave =
                _context.GameSaves.Single(save => save.GameSettingsId == gameId.Value);
            
            Game = new GameEngine.Game(gameSave);
            
            Board = Game.GetBoard();
            
            return Page();
        }
        
        public async Task<IActionResult> OnPostAsync(int? gameId, int? cellY, int? cellX, bool? flag)
        {
            if (gameId == null)
            {
                return RedirectToPage("./StartGame");
            }

            GameId = gameId.Value;

            var gameSave =
                _context.GameSaves.Single(save => save.GameSettingsId == gameId.Value);
            
            Game = new GameEngine.Game(gameSave);

            if (cellY != null && cellX != null && flag != null)
            {
                if (flag.Value)
                {
                    Game.Flag(cellY.Value, cellX.Value);
                }
                else
                {
                    Game.Move(cellY.Value, cellX.Value);
                }

                gameSave.Board = GameBoardJsonConverter.Serialize(Game.GetBoard());
                gameSave.FlagCount = Game.FlagCount;
                gameSave.RevealedCellAmount = Game.RevealedCellAmount;
                gameSave.GameOver = Game.GameOver;
                gameSave.PlayerWon = Game.PlayerWon;

                _context.GameSaves.Update(gameSave);
                
                await _context.SaveChangesAsync();
            }
            Board = Game.GetBoard();
            return Page();
        }

        public string GetMessage()
        {
            return Game.PlayerWon ? "You Won!" : "Game over!";
        }
    }
}