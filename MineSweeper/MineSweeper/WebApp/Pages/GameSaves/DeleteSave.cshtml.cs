﻿using System.Threading.Tasks;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Game
{
    public class DeleteSave : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public DeleteSave(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GameSave GameSave { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameSave = await _context.GameSaves.FirstOrDefaultAsync(save => save.GameSettingsId == id);

            if (GameSave == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameSave = await _context.GameSaves.FindAsync(id);

            if (GameSave != null)
            {
                _context.GameSaves.Remove(GameSave);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./SavedGames");
        }
    }
}