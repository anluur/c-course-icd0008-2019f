﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using GameEngine;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Game
{
    public class SavedGames : PageModel
    {

        private readonly AppDbContext _context;

        public SavedGames(AppDbContext context)
        {
            _context = context;
        }

        public IList<GameSave> Saves { get;set; }

        public async Task OnGetAsync()
        {
            Saves = await _context.GameSaves.OrderBy(save => save.SaveName).ToListAsync();
        }
    }
}