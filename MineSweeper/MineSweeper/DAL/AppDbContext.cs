﻿using System;
using System.IO;
using GameEngine;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {

        public DbSet<GameSave> GameSaves { get; set; } = default!;

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Data Source=./ConsoleApp/gameSaves.db";
            var builder = new SqliteConnectionStringBuilder(connectionString);               
            builder.DataSource = Path.GetFullPath(
                Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..//..//..//..//"),
                    builder.DataSource));
            connectionString = builder.ToString();
            
            optionsBuilder.UseSqlite(connectionString);
        }
        
        
        public AppDbContext(DbContextOptions options): base(options)
        {
        }

        public AppDbContext()
        {
            
        }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<GameSave>()
                .HasIndex(save => save.SaveName)
                .IsUnique(true);
        }
    }
}