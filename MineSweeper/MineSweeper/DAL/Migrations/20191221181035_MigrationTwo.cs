﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class MigrationTwo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "GameOver",
                table: "GameSaves",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "PlayerWon",
                table: "GameSaves",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_GameSaves_SaveName",
                table: "GameSaves",
                column: "SaveName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_GameSaves_SaveName",
                table: "GameSaves");

            migrationBuilder.DropColumn(
                name: "GameOver",
                table: "GameSaves");

            migrationBuilder.DropColumn(
                name: "PlayerWon",
                table: "GameSaves");
        }
    }
}
