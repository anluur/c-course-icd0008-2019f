﻿using System;
using System.ComponentModel;
using GameEngine;

namespace ConsoleUI
{
    public static class GameUI
    {
        private static readonly string _verticalSeparator = "|";
        private static readonly string _horizontalSeparator = "-";
        private static readonly string _centerSeparator = "+";
        
        public static void PrintBoard(Game game)
        {
            var board = game.GetBoard();
            
            var line = _centerSeparator;
            line += _horizontalSeparator + _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
            line += _centerSeparator;
            for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
            {
                line += _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
                line += _centerSeparator;
            }
            
            Console.WriteLine(line);

            line = _verticalSeparator + "    " + _verticalSeparator;
            for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
            {
                line += " ";
                line += (xIndex + 1).ToString();
                line += (xIndex + 1).ToString().Length == 1 ? " " + _verticalSeparator : _verticalSeparator;
            }

            Console.WriteLine(line);
            
            for (int yIndex = 0; yIndex <= game.BoardHeight; yIndex++)
            {
                
                line = _centerSeparator;
                line += _horizontalSeparator + _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
                line += _centerSeparator;
                for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
                {
                    line += _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
                    line += _centerSeparator;
                }
                
                Console.WriteLine(line);

                if (yIndex < game.BoardHeight)
                {
                    line = (yIndex + 1).ToString().Length == 1 ? _verticalSeparator + "  " : _verticalSeparator + " ";
                    line += (yIndex + 1).ToString() + " " + _verticalSeparator;
                    for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
                    {
                        line += " " + GetSingleState(board?[yIndex, xIndex]) + " ";
                        if (xIndex < game.BoardWidth)
                        {
                            line += _verticalSeparator;
                        }
                    }
                
                    Console.WriteLine(line);
                }
            }
        }

        private static string GetSingleState(Cell? cell)
        {
            switch (cell?.State)
            {
                case CellState.Hidden:
                    return "█";
                case CellState.Flagged:
                    return "F";
                case CellState.Revealed:
                    if (cell?.IsMine == true)
                    {
                        return "@";
                    }
                    else
                    {
                        return cell?.MinesAround! == 0 ? " " : cell?.MinesAround!.ToString();
                    }
                default:
                    throw new InvalidEnumArgumentException("Unknown enum option!");
            }
            
        }
    }
}