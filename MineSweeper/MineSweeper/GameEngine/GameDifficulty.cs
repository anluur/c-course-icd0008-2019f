﻿namespace GameEngine
{
    public enum GameDifficulty
    {
        Easy,    // 9, 9, 10
        Medium,  // 16, 16, 40
        Hard,     // 16, 30, 99
        Custom
    }
}