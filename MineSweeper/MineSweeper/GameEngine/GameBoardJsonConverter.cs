﻿using Newtonsoft.Json;

namespace GameEngine
{
    public class GameBoardJsonConverter
    {
        public static string Serialize(Cell[,] gameBoard)
        {
            var jsonString = JsonConvert.SerializeObject(gameBoard, Formatting.Indented);
            return jsonString;
        }

        public static Cell[,] Deserialize(string jsonString)
        {
            var result = JsonConvert.DeserializeObject<Cell[,]>(jsonString);
            return result;
        }
    }
}