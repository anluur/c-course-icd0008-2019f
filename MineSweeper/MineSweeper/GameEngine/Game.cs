﻿using System;

namespace GameEngine
{
    public class Game
    {
        private Cell[,] Board { get; set; }
        public int BoardHeight { get; }
        public int BoardWidth { get; }
        public int MineAmount { get; }

        public bool GameOver { get; set; } = false;
        public bool PlayerWon { get; set; } = false;
        public int RevealedCellAmount { get; set; } = 0;

        public int FlagCount { get; set; }

        public Game(GameDifficulty difficulty = GameDifficulty.Easy)
        {
            switch (difficulty)
            {
                case GameDifficulty.Easy:
                    BoardHeight = 9;
                    BoardWidth = 9;
                    MineAmount = 10;
                    break;
                case GameDifficulty.Medium:
                    BoardHeight = 16;
                    BoardWidth = 16;
                    MineAmount = 40;
                    break;
                case GameDifficulty.Hard:
                    BoardHeight = 16;
                    BoardWidth = 30;
                    MineAmount = 99;
                    break;
            }

            Board = new Cell[BoardHeight, BoardWidth];
            
            SetMines();
        }

        public Game(int boardHeight, int boardWidth, int mineAmount)
        {
            BoardHeight = boardHeight;
            BoardWidth = boardWidth;
            MineAmount = mineAmount <= boardHeight * boardWidth - 1 ? 
                mineAmount 
                : boardHeight * boardWidth - 1 ;

            // SETUP
            if (BoardHeight < 9 || BoardWidth < 9)
            {
                throw new ArgumentException("Board size should be equal or bigger than 9x9.");
            }
            
            Board = new Cell[BoardHeight, BoardWidth];
            
            SetMines();
        }

        private void SetMines()
        {
            //fill Board with mines randomly.
            Random rand = new Random();

            int i = 0;
            while (i < MineAmount)
            {
                var posY = rand.Next(BoardHeight);
                var posX = rand.Next(BoardWidth);
                if (Board[posY, posX].IsMine == false)
                {
                    Board[posY, posX].IsMine = true;
                    i++;
                }
            }
        }


        public Game(GameSave save)
        {
            if (save.BoardHeight < 9 || save.BoardWidth < 9)
            {
                throw new ArgumentException("Board size should be equal or bigger than 9x9.");
            }
            BoardHeight = save.BoardHeight;
            BoardWidth = save.BoardWidth;
            MineAmount = save.MineCount;
            Board = GameBoardJsonConverter.Deserialize(save.Board);
            RevealedCellAmount = save.RevealedCellAmount;
            FlagCount = save.FlagCount;
            GameOver = save.GameOver;
            PlayerWon = save.PlayerWon;

        }
        
        public Cell[,] GetBoard()
        {
            var result = new Cell[BoardHeight, BoardWidth];
            if (Board != null)
            {
                Array.Copy(Board, result, Board.Length);
            }
            else
            {
                throw new NullReferenceException("Board is null");
            }
            return result;
        }

        public void Flag(int posY, int posX)
        {
            if (GameOver)
            {
                return;
            }
            
            if (Board[posY, posX].State == CellState.Flagged)
            {
                Board[posY, posX].State = CellState.Hidden;
                FlagCount--;
            }
            else if (Board[posY, posX].State == CellState.Hidden && FlagCount < MineAmount)
            {
                Board[posY, posX].State = CellState.Flagged;
                FlagCount++;
            }

        }
        
        public void Move(int posY, int posX)
        {
            if (GameOver)
            {
                return;
            }
            if (Board[posY, posX].State == CellState.Revealed)
            {
                return;
            }
            else if (Board[posY, posX].State == CellState.Flagged)
            {
                FlagCount--;
            }
            
            Board[posY, posX].State = CellState.Revealed;
            RevealedCellAmount++;

            // Player lost
            if (Board[posY, posX].IsMine)
            {
                RevealAllMines();
                GameOver = true;
            }
            else
            {
                SetAdjacentMineCount(posY, posX);
                RevealAdjacent(posY, posX);

                // Player won
                if (RevealedCellAmount >= BoardHeight * BoardWidth - MineAmount)
                {
                    GameOver = true;
                    PlayerWon = true;
                }
            }
        }

        private void SetAdjacentMineCount(int posY, int posX)
        {
            int mineCount = 0;

            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    if (y == 0 && x == 0) continue;
                    
                    if (InBounds(posY + y, posX + x))
                    {
                        if (Board[posY + y, posX + x].IsMine == true)
                        {
                            mineCount++;
                        }
                    }
                }
            }

            if (Board != null) Board[posY, posX].MinesAround = mineCount;
        }

        private void RevealAllMines()
        {
            for (int yIndex = 0; yIndex < BoardHeight; yIndex++)
            {
                for (int xIndex = 0; xIndex < BoardWidth; xIndex++)
                {
                    if (Board[yIndex, xIndex].IsMine == true)
                    {
                        Board[yIndex, xIndex].State = CellState.Revealed;
                    }
                }
            }
        }

        private void RevealAdjacent(int posY, int posX)
        {
            if (Board?[posY, posX].MinesAround == 0)
            {
                for (int y = -1; y <= 1; y++)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        if (y == 0 && x == 0) continue;
                        if (InBounds(posY + y, posX + x))
                        {
                            if (!Board[posY + y, posX + x].IsMine && Board[posY + y, posX + x].State != CellState.Revealed)
                            {
                                if (Board[posY + y, posX + x].State != CellState.Flagged)
                                {
                                    FlagCount--;
                                }
                                Board[posY + y, posX + x].State = CellState.Revealed;
                                RevealedCellAmount++;
                                SetAdjacentMineCount(posY + y, posX + x);
                                if (Board[posY + y, posX + x].MinesAround == 0)
                                {
                                    RevealAdjacent(posY + y, posX + x);
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool InBounds(int posY, int posX)
        {
            return posY >= 0 && posY < BoardHeight && posX >= 0 && posX < BoardWidth;
        }
    }
}