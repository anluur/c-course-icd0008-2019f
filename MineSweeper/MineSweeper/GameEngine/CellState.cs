﻿namespace GameEngine
{
    public enum CellState
    {
        Hidden,          // = 0
        Revealed,        // = 1   
        Flagged          // = 2
    }
}