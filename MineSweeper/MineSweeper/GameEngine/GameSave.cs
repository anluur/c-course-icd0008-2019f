﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameEngine
{
    public class GameSave
    {

        [Key] public int GameSettingsId { get; set; }
        [Required] [Display(Name = "Name")] public string SaveName { get; set; } = default!;
        [Required] [Range(9, 50)] [Display(Name = "Board height")] public int BoardHeight { get; set; }
        [Required] [Range(9, 50)] [Display(Name = "Board width")] public int BoardWidth { get; set; }
        [Required] [Range(1, Int32.MaxValue)] [Display(Name = "Mine amount")] public int MineCount { get; set; }
        [Required] public string Board { get; set; } = default!;
        [Required] public int FlagCount { get; set; }
        [Required] public int RevealedCellAmount { get; set; }
        [Required] [Display(Name = "Game over")] public bool GameOver { get; set; }
        [Required] public bool PlayerWon { get; set; }
    }
}