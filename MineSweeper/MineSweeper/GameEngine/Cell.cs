﻿namespace GameEngine
{
    public struct Cell
    {
        public bool IsMine { get; set; }
        public CellState State { get; set; }

        public int MinesAround { get; set; }

        public Cell(bool isMine, CellState state)
        {
            this.IsMine = isMine;
            this.State = state;
            this.MinesAround = -1;
        }
    }
}