﻿using System;
using System.IO;
using Domain;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; } = default!;
        public DbSet<FoodCategory> FoodCategories { get; set; } = default!;
        public DbSet<ProductInRecipe> ProductsInRecipe { get; set; } = default!;
        public DbSet<ProductInHand> ProductsInHand { get; set; } = default!;
        public DbSet<User> Users { get; set; } = default!;
        public DbSet<Recipe> Recipes { get; set; } = default!;
        public DbSet<RecipeCategory> RecipeCategories { get; set; } = default!;
        public DbSet<Unit> Units { get; set; } = default!;
        
        public AppDbContext(DbContextOptions options) : base()
        {
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Data Source=./examdb.db";
            var builder = new SqliteConnectionStringBuilder(connectionString);               
            builder.DataSource = Path.GetFullPath(
                Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..//..//..//..//"),
                    builder.DataSource));
            connectionString = builder.ToString();
            
            optionsBuilder.UseSqlite(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Unit>().HasIndex(u => u.Name).IsUnique();
            modelBuilder.Entity<Product>().HasIndex(p => p.Name).IsUnique();
            modelBuilder.Entity<FoodCategory>().HasIndex(fc => fc.Name).IsUnique();
            modelBuilder.Entity<Recipe>().HasIndex(r => r.Name).IsUnique();
            modelBuilder.Entity<RecipeCategory>().HasIndex(rc => rc.Name).IsUnique();
        }
    }
}