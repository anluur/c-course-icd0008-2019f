﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Recipe
    {
        [Key]
        public int RecipeId { get; set; }

        [Required] [Display(Name="Recipe Category")] public int RecipeCategoryId { get; set; } = default!;
        [Display(Name="Recipe Category")] public RecipeCategory? RecipeCategory { get; set; }

        [Display(Name="Recipe name")]
        [Required]
        [MaxLength(64, ErrorMessage = "Recipe name too long!")]
        [MinLength(2, ErrorMessage = "Recipe name too short!")]
        public string Name { get; set; } = default!;

        [Display(Name="Ingredients")]
        public ICollection<ProductInRecipe>? ProductsInRecipe { get; set; }

        [Display(Name="Instructions")]
        [Required]
        [MaxLength(1024, ErrorMessage = "Instructions too long!")]
        [MinLength(2, ErrorMessage = "Instrutions too short!")]
        public string Instructions { get; set; }

        [Required]
        [Display(Name="Time needed (in minutes)")]
        [Range(5, Int32.MaxValue, ErrorMessage = "Invalid time amount")]
        public int TimeNeeded { get; set; } // in minutes

        [Required]
        [Display(Name = "Servings")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid number of servings")]
        public int Servings { get; set; }
    }
}