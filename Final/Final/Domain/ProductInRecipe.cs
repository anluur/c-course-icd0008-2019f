﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;

namespace Domain
{
    public class ProductInRecipe
    {
        [Key]
        public int ProductInRecipeId { get; set; }

        [Required]
        public int ProductId { get; set; } = default!;

        public Product? Product { get; set; }

        [Required]
        public int RecipeId { get; set; } = default!;
        
        public Recipe? Recipe { get; set; }

        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid product amount")]
        public int Amount { get; set; } = default!;

        [Required]
        public int UnitId { get; set; } = default!;

        public Unit? Unit { get; set; }

        public string ToString(int servings, int defaultServings)
        {
            return (int)(Amount * ((float)servings / defaultServings)) + " " + Unit.Name + " " + Product.Name;
        }
    }
}