﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Display(Name="Product name")]
        [Required]
        [MaxLength(64, ErrorMessage = "Product name too long!")]
        [MinLength(2, ErrorMessage = "Product name too short!")]
        public string Name { get; set; } = default!;

        [Required] 
        [Display(Name="Food Category")]
        public int FoodCategoryId { get; set; } = default!;
        public FoodCategory? Category { get; set; }
        
    }
}