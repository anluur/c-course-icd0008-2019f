﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class ProductInHand
    {
        [Key]
        public int ProductInHandId { get; set; }

        [Required] 
        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid product amount")]
        public int Amount { get; set; } = default!;

        [Required] public int ProductId { get; set; } = default!;
        public Product? Product { get; set; }

        [Required] [Display(Name="User")] public int UserId { get; set; } = default!;
        public User? User { get; set; }

        [Required] public int UnitId { get; set; } = default!;

        public Unit? Unit { get; set; }
        
        public int CompareAmountTo(ProductInRecipe recipeProduct, int? Servings)
        {
            var thisUnit = this.Unit;
            var otherUnit = recipeProduct.Unit;
            if (thisUnit.Name == otherUnit.Name)
            {
                return Servings != null ? 
                    this.Amount.CompareTo((int)((float)recipeProduct.Amount / recipeProduct.Recipe.Servings * Servings.Value))
                    : this.Amount.CompareTo(recipeProduct.Amount);
            }

            return -1;
        }
    }
}