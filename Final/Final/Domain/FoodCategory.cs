﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class FoodCategory
    {
        [Key]
        public int FoodCategoryId { get; set; }

        [Display(Name="Food Category name")]
        [Required]
        [MaxLength(64, ErrorMessage = "Food category name too long!")]
        [MinLength(2, ErrorMessage = "Food category name too short!")]
        public string Name { get; set; } = default!;

        public ICollection<Product>? Products { get; set; }
    }
}