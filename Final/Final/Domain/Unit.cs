﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Unit
    {
        [Key]
        public int UnitId { get; set; }

        [Display(Name="Unit")]
        [Required]
        [MaxLength(64, ErrorMessage = "Unit name too long!")]
        [MinLength(2, ErrorMessage = "Unit name too short!")]
        public string Name { get; set; } = default!;
    }
}