﻿﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        
        [Display(Name="User name")]
        [Required]
        [MaxLength(64, ErrorMessage = "User name too long!")]
        [MinLength(2, ErrorMessage = "User name too short!")]
        public string Name { get; set; } = default!;

        public ICollection<ProductInHand>? ProductsInHand { get; set; }
    }
}