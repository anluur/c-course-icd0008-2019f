﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class RecipeCategory
    {
        [Key]
        public int RecipeCategoryId { get; set; }

        [Display(Name="Recipe Category name")]
        [Required]
        [MaxLength(64, ErrorMessage = "Recipe category name too long!")]
        [MinLength(2, ErrorMessage = "Recipe category name too short!")]
        public string Name { get; set; } = default!;

        public ICollection<Recipe>? Recipes { get; set; }
    }
}