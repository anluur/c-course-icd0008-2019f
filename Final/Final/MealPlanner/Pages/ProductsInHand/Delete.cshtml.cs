using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_ProductsInHand
{
    public class DeleteModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public DeleteModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ProductInHand ProductInHand { get; set; }
        
        [BindProperty]
        public int UserId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInHand = await _context.ProductsInHand
                .Include(p => p.Product)
                .Include(p => p.Unit)
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.ProductInHandId == id);

            if (ProductInHand == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInHand = await _context.ProductsInHand.FindAsync(id);

            UserId = ProductInHand.UserId;

            if (ProductInHand != null)
            {
                _context.ProductsInHand.Remove(ProductInHand);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("../Users/Details", new {id = UserId});
        }
    }
}
