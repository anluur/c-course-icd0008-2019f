using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace MealPlanner.Pages_ProductsInHand
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList ProductSelectList { get; set; }
        public SelectList UnitSelectList { get; set; }

        public int UserId { get; set; }
        
        public async Task<IActionResult> OnGet(int? userId)
        {
            if (userId == null)
            {
                return NotFound();
            }
            ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                    nameof(Product.ProductId), 
                    nameof(Product.Name)); 
            UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                    nameof(Unit.UnitId), 
                    nameof(Unit.Name));

            User = await _context.Users.FirstOrDefaultAsync(u => u.UserId == userId.Value);

            if (User == null)
            {
                return NotFound();
            }

            UserId = userId.Value;
            
            return Page();
        }

        public new User User { get; set; }        

        [BindProperty]
        public ProductInHand ProductInHand { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                    nameof(Product.ProductId), 
                    nameof(Product.Name)); 
                UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                    nameof(Unit.UnitId), 
                    nameof(Unit.Name));
                
                return Page();
            }

            var existing = _context.ProductsInHand
                .Include(p => p.Product)
                    .ThenInclude(p => p.Category)
                .Include(p => p.Unit)
                .Include(p => p.User)
                .Single(p => p.UnitId == ProductInHand.UnitId &&
                            p.ProductId == ProductInHand.ProductId &&
                            p.UserId == ProductInHand.UserId);
            
            
            if (existing != null)
            {
                existing.Amount += ProductInHand.Amount;
                _context.ProductsInHand.Update(existing);
            }
            else
            {
                _context.ProductsInHand.Add(ProductInHand);
            }
            await _context.SaveChangesAsync();

            return RedirectToPage("../Users/Details", new {id = ProductInHand.UserId});
        }
    }
}
