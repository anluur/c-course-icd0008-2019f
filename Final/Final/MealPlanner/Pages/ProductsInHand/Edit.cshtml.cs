using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_ProductsInHand
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ProductInHand ProductInHand { get; set; }

        public SelectList ProductSelectList { get; set; }
        public SelectList UnitSelectList { get; set; }
        
        public int UserId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInHand = await _context.ProductsInHand
                .Include(p => p.Product)
                .Include(p => p.Unit)
                .Include(p => p.User).FirstOrDefaultAsync(m => m.ProductInHandId == id);

            if (ProductInHand == null)
            {
                return NotFound();
            }

            UserId = ProductInHand.UserId;
            
           ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
               nameof(Product.ProductId), nameof(Product.Name));
           UnitSelectList = new SelectList(_context.Units, 
               nameof(Unit.UnitId), nameof(Unit.Name));
           
           return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                    nameof(Product.ProductId), nameof(Product.Name));
                UnitSelectList = new SelectList(_context.Units, 
                    nameof(Unit.UnitId), nameof(Unit.Name));
                
                return Page();
            }

            _context.Attach(ProductInHand).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductInHandExists(ProductInHand.ProductInHandId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("../Users/Details", new {id = ProductInHand.UserId});
        }

        private bool ProductInHandExists(int id)
        {
            return _context.ProductsInHand.Any(e => e.ProductInHandId == id);
        }
    }
}
