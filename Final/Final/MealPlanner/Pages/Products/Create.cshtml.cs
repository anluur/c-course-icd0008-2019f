using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;

namespace MealPlanner.Pages_Products
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList FoodCategorySelectList { get; set; }
        
        public IActionResult OnGet()
        {
        FoodCategorySelectList = new SelectList(_context.FoodCategories.OrderBy(x => x.Name),
            nameof(FoodCategory.FoodCategoryId), nameof(FoodCategory.Name));
            return Page();
        }

        [BindProperty]
        public Product Product { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                FoodCategorySelectList = new SelectList(_context.FoodCategories.OrderBy(x => x.Name),
                    nameof(FoodCategory.FoodCategoryId), nameof(FoodCategory.Name));
                
                return Page();
            }
            

            _context.Products.Add(Product);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
