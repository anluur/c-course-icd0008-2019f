using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_RecipeCategories
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<RecipeCategory> RecipeCategory { get;set; }

        public async Task OnGetAsync()
        {
            RecipeCategory = await _context.RecipeCategories.OrderBy(x => x.Name).ToListAsync();
        }
    }
}
