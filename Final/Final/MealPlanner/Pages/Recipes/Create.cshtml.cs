using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;

namespace MealPlanner.Pages_Recipes
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public SelectList RecipeCategorySelectList { get; set; }
        
        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            RecipeCategorySelectList = new SelectList(_context.RecipeCategories.OrderBy(x => x.Name), 
                nameof(RecipeCategory.RecipeCategoryId),
                nameof(RecipeCategory.Name));
            return Page();
        }

        [BindProperty]
        public Recipe Recipe { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                RecipeCategorySelectList = new SelectList(_context.RecipeCategories.OrderBy(x => x.Name), 
                    nameof(RecipeCategory.RecipeCategoryId),
                    nameof(RecipeCategory.Name));
                return Page();
            }

            _context.Recipes.Add(Recipe);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
