﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MealPlanner.Pages_Recipes
{
    public class Ingredient : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public Ingredient(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<ProductInRecipe> ProductInRecipe { get;set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInRecipe = await _context.ProductsInRecipe
                .Where(pr => pr.RecipeId == id.Value)
                .Include(p => p.Product)
                .Include(p => p.Recipe)
                .Include(p => p.Unit)
                .OrderBy(x => x.Product.Name)
                .ToListAsync();

            if (ProductInRecipe.Count == 0)
            {
                return RedirectToPage("../ProductsInRecipes/Create", new {id = id});
            }

            return Page();
        }
    }
}