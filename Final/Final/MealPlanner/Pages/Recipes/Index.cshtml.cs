using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_Recipes
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Recipe> Recipes { get;set; }
        public IList<ProductInHand> UserProducts { get;set; }

        [Display(Name = "Include Search")]
        public string IncludeSearch { get; set; }
        
        [Display(Name = "Exclude Search")]
        public string ExcludeSearch { get; set; }
        
        [Range(1, 20, ErrorMessage = "Servings number should be in range 1 to 20")]
        public int? Servings { get; set; }
        [Range(5, 2880, ErrorMessage = "Time should be in range 5 to 2880 minutes")]
        public int? Time { get; set; }

        public int? UserId { get; set; }
        public string UserName { get; set; }

        public async Task OnGetAsync(int? userId, string? includeSearch, string? excludeSearch, int? time, int? servings)
        {
            if (servings != null)
            {
                Servings = servings;
            }
            
            if (userId != null)
            {
                UserName = _context.Users.Single(u => u.UserId == userId.Value).Name;

                UserProducts = await _context.ProductsInHand
                    .Where(up => up.UserId == userId.Value)
                    .Include(up => up.Product)
                    .Include(up => up.Unit)
                    .ToListAsync();

                var recipes = await _context.Recipes
                    .Include(r => r.RecipeCategory)
                    .Include(r => r.ProductsInRecipe)
                    .ThenInclude(pr => pr.Product)
                    .ThenInclude(p => p.Category)
                    .Include(r => r.ProductsInRecipe)
                    .ThenInclude(pr => pr.Unit)
                    .Include(r => r.ProductsInRecipe)
                    .OrderBy(x => x.Name)
                    .ToListAsync();
                
                Recipes = new List<Recipe>();

                foreach (var recipe in recipes)
                {
                    var flag = 0;
                    foreach (var recipeProduct in recipe.ProductsInRecipe)
                    {
                        foreach (var userProduct in UserProducts)
                        {
                            if (userProduct.ProductId == recipeProduct.ProductId)
                            {
                                if (userProduct.CompareAmountTo(recipeProduct, Servings) >= 0)
                                {
                                    flag++;
                                    if (flag >= recipe.ProductsInRecipe.Count)
                                    {
                                        Recipes.Add(recipe);
                                        break;
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Recipes = await _context.Recipes
                    .Include(r => r.RecipeCategory)
                    .Include(r => r.ProductsInRecipe)
                    .ThenInclude(pr => pr.Product)
                    .ThenInclude(p => p.Category)
                    .Include(r => r.ProductsInRecipe)
                    .OrderBy(x => x.Name)
                    .ToListAsync();
            }
            
            if (!String.IsNullOrWhiteSpace(includeSearch) && String.IsNullOrWhiteSpace(excludeSearch))
            {
                IncludeSearch = includeSearch;
                includeSearch = includeSearch.ToLower().Trim();

                Recipes = Recipes.Where(recipe => recipe.Name.ToLower().Contains(includeSearch) || 
                                   recipe.Instructions.ToLower().Contains(includeSearch) ||
                                   recipe.RecipeCategory.Name.ToLower().Contains(includeSearch) ||
                                   recipe.ProductsInRecipe.Any(pr => pr.Product.Name.ToLower().Contains(includeSearch) ||
                                                                     pr.Product.Category.Name.ToLower().Contains(includeSearch)
                                   )
                    )
                    .ToList();
            } 
            else if (String.IsNullOrWhiteSpace(includeSearch) && !String.IsNullOrWhiteSpace(excludeSearch))
            {
                Console.Out.WriteLine("HOH");
                ExcludeSearch = excludeSearch;
                excludeSearch = excludeSearch.ToLower().Trim();

                Recipes = Recipes.Where(recipe => !(recipe.Name.ToLower().Contains(excludeSearch) || 
                                  recipe.Instructions.ToLower().Contains(excludeSearch) ||
                                  recipe.RecipeCategory.Name.ToLower().Contains(excludeSearch) ||
                                  recipe.ProductsInRecipe.Any(pr => pr.Product.Name.ToLower().Contains(excludeSearch) ||
                                                                    pr.Product.Category.Name.ToLower().Contains(excludeSearch)
                                  ))
                    )
                    .ToList();
            } 
            else if (!String.IsNullOrWhiteSpace(includeSearch) && !String.IsNullOrWhiteSpace(excludeSearch))
            {
                IncludeSearch = includeSearch;
                includeSearch = includeSearch.ToLower().Trim();
                ExcludeSearch = excludeSearch;
                excludeSearch = excludeSearch.ToLower().Trim();

                Recipes = Recipes.Where(recipe => 
                        (recipe.Name.ToLower().Contains(includeSearch) || 
                        recipe.Instructions.ToLower().Contains(includeSearch) ||
                        recipe.RecipeCategory.Name.ToLower().Contains(includeSearch) ||
                        recipe.ProductsInRecipe.Any(pr => pr.Product.Name.ToLower().Contains(includeSearch) ||
                                                          pr.Product.Category.Name.ToLower().Contains(includeSearch)
                                                    )
                        ) &&
                        !(recipe.Name.ToLower().Contains(excludeSearch) ||
                          recipe.Instructions.ToLower().Contains(excludeSearch) ||
                          recipe.RecipeCategory.Name.ToLower().Contains(excludeSearch) || 
                          recipe.ProductsInRecipe.Any(pr => pr.Product.Name.ToLower().Contains(excludeSearch) || 
                                                            pr.Product.Category.Name.ToLower().Contains(excludeSearch)
                                                            )
                          )
                        
                    )
                    .ToList();
            }

            if (time != null)
            {
                Time = time;
                Recipes = Recipes.Where(r => r.TimeNeeded <= time).ToList();
            }

            if (servings != null)
            {
                Servings = servings;
            }
        }
    }
}
