using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_Recipes
{
    public class DetailsModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public DetailsModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Recipe Recipe { get; set; }

        [BindProperty]
        public int Servings { get; set; }

        [BindProperty]
        public int Id { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, int? servings)
        {
            if (id == null)
            {
                return NotFound();
            }

            Recipe = await _context.Recipes
                .Include(r => r.RecipeCategory)
                .Include(r => r.ProductsInRecipe)
                    .ThenInclude(pr => pr.Product)
                .Include(r => r.ProductsInRecipe)
                    .ThenInclude(pr => pr.Unit)
                .FirstOrDefaultAsync(m => m.RecipeId == id);

            if (Recipe == null)
            {
                return NotFound();
            }
            
            if (servings == null)
            {
                servings = Recipe.Servings;
            }

            Servings = servings.Value;
            
            return Page();
        }

        public IActionResult OnPost()
        {
            return RedirectToPage(new {id = Id, servings = Servings});
        }
    }
}
