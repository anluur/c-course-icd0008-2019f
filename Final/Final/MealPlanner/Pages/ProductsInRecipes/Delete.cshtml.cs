using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_ProductsInRecipes
{
    public class DeleteModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public DeleteModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ProductInRecipe ProductInRecipe { get; set; }

        public int RecipeId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInRecipe = await _context.ProductsInRecipe
                .Include(p => p.Product)
                .Include(p => p.Recipe)
                .Include(p => p.Unit).FirstOrDefaultAsync(m => m.ProductInRecipeId == id);

            if (ProductInRecipe == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInRecipe = await _context.ProductsInRecipe.FindAsync(id);

            RecipeId = ProductInRecipe.RecipeId;
            
            if (ProductInRecipe != null)
            {
                _context.ProductsInRecipe.Remove(ProductInRecipe);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("../Recipes/Ingredient", new {id = RecipeId});
        }
    }
}
