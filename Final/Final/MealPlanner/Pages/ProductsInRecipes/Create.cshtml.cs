using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace MealPlanner.Pages_ProductsInRecipes
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList ProductSelectList { get; set; }
        public SelectList UnitSelectList { get; set; }

        public Recipe Recipe { get; set; }
        public int RecipeId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                nameof(Product.ProductId), 
                nameof(Product.Name));
            UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                nameof(Unit.UnitId), 
                nameof(Unit.Name));
            
            Recipe = await _context.Recipes.FirstOrDefaultAsync(u => u.RecipeId == id.Value);

            if (Recipe == null)
            {
                return NotFound();
            }

            RecipeId = id.Value;
        
            return Page();
        }

        [BindProperty]
        public ProductInRecipe ProductInRecipe { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                    nameof(Product.ProductId), 
                    nameof(Product.Name));
                UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                    nameof(Unit.UnitId), 
                    nameof(Unit.Name));
                
                return Page();
            }

            _context.ProductsInRecipe.Add(ProductInRecipe);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Recipes/Ingredient", new {id = ProductInRecipe.RecipeId});
        }
    }
}
