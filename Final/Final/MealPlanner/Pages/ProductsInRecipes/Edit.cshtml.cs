using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_ProductsInRecipes
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ProductInRecipe ProductInRecipe { get; set; }
        
        public SelectList ProductSelectList { get; set; }
        public SelectList UnitSelectList { get; set; }

        public int RecipeId { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductInRecipe = await _context.ProductsInRecipe
                .Include(p => p.Product)
                .Include(p => p.Recipe)
                .Include(p => p.Unit).FirstOrDefaultAsync(m => m.ProductInRecipeId == id);

            if (ProductInRecipe == null)
            {
                return NotFound();
            }

            RecipeId = ProductInRecipe.RecipeId;

            ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                nameof(Product.ProductId), 
                nameof(Product.Name));
            UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                nameof(Unit.UnitId), 
                nameof(Unit.Name));
            
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ProductSelectList = new SelectList(_context.Products.OrderBy(x => x.Name), 
                    nameof(Product.ProductId), 
                    nameof(Product.Name));
                UnitSelectList = new SelectList(_context.Units.OrderBy(x => x.Name), 
                    nameof(Unit.UnitId), 
                    nameof(Unit.Name));
                
                return Page();
            }

            _context.Attach(ProductInRecipe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductInRecipeExists(ProductInRecipe.ProductInRecipeId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("../Recipes/Ingredient", new {id = ProductInRecipe.RecipeId});
        }

        private bool ProductInRecipeExists(int id)
        {
            return _context.ProductsInRecipe.Any(e => e.ProductInRecipeId == id);
        }
    }
}
