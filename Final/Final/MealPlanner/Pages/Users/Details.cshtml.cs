﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace MealPlanner.Pages_Users
{
    public class DetailsModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public DetailsModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public new User User { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User = await _context.Users
                .Include(u => u.ProductsInHand)
                    .ThenInclude(u => u.Product)
                .Include(u => u.ProductsInHand)
                    .ThenInclude(u => u.Unit)
                .FirstOrDefaultAsync(m => m.UserId == id);

            if (User == null)
            {
                return NotFound();
            }

            ProductsInHand = User.ProductsInHand.ToList();
            
            return Page();
        }

        public IList<ProductInHand> ProductsInHand { get; set; }
    }
}
