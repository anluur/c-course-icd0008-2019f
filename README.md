# icd0008-2019f

Projects developed during C# course. Main technologies: EF Core, SQLite and ASP.NET Razor Pages.

Minesweeper - course project: minesweeper game engine 
with ability to save and load games from database.
Console app and web app for ui.

Final - final exam project developed in less than 24 hours. Basically mealplanning system.


## Run

To run apps clone project. Open desired project directory find solution file (.sln) 
and open it with Jetbrains Rider or other C# IDE. Run project.